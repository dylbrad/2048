import { placeRandomNumber, createEmptyGrid } from "lib/grid";

function GameOverPopup(props) {
  function handleClick() {
    let newGrid = createEmptyGrid();
    props.setGrid(placeRandomNumber(newGrid));
    props.setShowGameOver(false);
  }

  return (
    <div className="game-over-wrapper">
      <div className="game-over-content">
        <h2>GAME OVER</h2>
        <button onClick={handleClick}>Play again?</button>
      </div>
    </div>
  );
}

export default GameOverPopup;
