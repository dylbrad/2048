import renderer from "react-test-renderer";
import Grid from "components/Grid";

it("renders Grid correctly", () => {
  const tree = renderer.create(<Grid />).toJSON();
  expect(tree).toMatchSnapshot();
});
